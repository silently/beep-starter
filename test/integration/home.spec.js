import { app, chai } from '../setup.js';

describe('Homepage', () => {
  describe('GET /', () => {
    it('should contain hello', async () => {
       const res = await chai.request(app).get('/');
       res.should.have.status(200);
       res.text.should.contain('hello');
     });
  });
});
