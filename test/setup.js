import chai from 'chai';
import chaiHttp from 'chai-http';
import app from 'build/app.js';

// Configure
chai.use(chaiHttp);
chai.should();

export { app, chai };
