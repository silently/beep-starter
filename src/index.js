import 'dotenv/config';
import db from 'db/index.js';
import app from 'app.js';

db.connect((err) => {
  if(err) return console.error('[pg] Error acquiring client', err.stack);
  return console.log('[pg] Connected to database');
});

// PORT is defined in .env configuration (see README)
app.listen(process.env.PORT, (err) => {
  if(err) return console.error('[express] Error starting server', err.stack);
  return console.log(`[express] Server is running on port ${process.env.PORT}`);
});
