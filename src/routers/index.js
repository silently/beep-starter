import express from 'express';
import homeRouter from 'routers/home.js';

const router = express.Router();
router.use('/', homeRouter);
// add new routers here

export default router;
