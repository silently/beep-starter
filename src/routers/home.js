import express from 'express';
import * as homeController from 'controllers/home.js'

// Map routes to controllers
// ------------------------------------------
const router = express.Router();
router.get('/', homeController.root);
router.get('/now', homeController.now);

export default router;
