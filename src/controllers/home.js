import { now as nowQuery } from 'db/queries/info.js';

const root = (req, res) => {
    res.send('hello');
};

const now = async (req, res, next) => {
    try {
        const { rows } = await nowQuery();
        res.json({ now: rows[0].now });
    } catch (err) {
        next(err);
    }
};

export { root, now };
