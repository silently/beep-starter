import db from 'db/index.js';

export const now = async () => await db.query('SELECT NOW()');
