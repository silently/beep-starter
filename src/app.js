import express from 'express';
import router from 'routers/index.js';

const app = express();

// Generic middleware
// ------------------------------------------
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// Add needed middleware: express.static, cookie parser, JWT validation, helmet...

// Routes
// ------------------------------------------
app.use('/', router);

// Error handling
// ------------------------------------------
app.use((err, req, res, next) => {
  console.log(err);
  res.status(err.status || 500);
});

export default app;
