# beep-starter

Express.js project starter (beep stands for babel-express-eslint-pg) with the following characteristics:

* compile `src/` (with [babel](https://babeljs.io/), for JS syntax and module resolver) to `build/`
* use PostgreSQL with [pg](https://github.com/brianc/node-postgres) (and configure locally with [dotenv](https://github.com/motdotla/dotenv))
* [mocha](https://github.com/mochajs/mocha), [chai](https://github.com/chaijs/chai) and [chai-http](https://github.com/chaijs/chai-http) for tests
* [eslint](https://eslint.org/) with minimal configuration (one may want to add IDE integration, like [linter-eslint](https://atom.io/packages/linter-eslint) for Atom)
* `.gitlab-ci.yml` configuration example for Gitlab CI/CD with Heroku

The typical request flow is `app -> routes -> controllers -> db/queries -> view` using `async/await` in controllers and queries.

Missing (among others): public directory served by `express.static`, structure for layouts, translations, database migrations, seeds and models...

## Installation

```shell
# Clone and reinit (git repo, npm package)
git clone git@gitlab.com:silently/beep-starter.git myapp
cd myapp
rm -rf .git && git init && npm init

# Install dependencies
npm install

# Configure (needs editing .env file with valid PostgreSQL settings)
cp .env.example .env
```

## Running

```shell
# Run src with nodemon
npm run dev

# Lint src folder
npm run lint

# Create build
npm run build

# Run build
npm run start

# Run mocha tests on build
npm run test

# Ensure build before starting or testing
npm run build && npm run start
npm run build && npm run test
```

## CI and CD to Heroku

1. Create an Heroku app

2. Set `HEROKU_APP` and `HEROKU_API_KEY` variables in the gitlab web interface (Settings / CI-CD / Variables)

2. Push to gitlab

## Notes

Remarks regarding CI/CD with Heroku:

* the `heroku-postbuild` script defined in `package.json` serves the only purpose to prevent Heroku from triggering the `build` one (indeed the `build/` repository is created and uploaded to Heroku by gitlab runners)
* Following that, one may `heroku config:set NPM_CONFIG_PRODUCTION=true --app APP_NAME` so that devDependencies are not installed by Heroku (would only be necessary to build). Read more about [Heroku build process](https://devcenter.heroku.com/articles/nodejs-support#build-behavior).
* More generally, deployment choices are tightly linked between `package.json`, `.gitlab-ci.yml` and environment variables defined in each environment (`NODE_ENV` in particular).

Now a word about the `.gitlab-ci.yml` configuration regarding `node_modules`. This folder may be cached and/or shared between jobs (through artifacts):

* the `cache` option is defined at the root of the configuration to be shared between jobs
* if `node_modules` is *not* shared through `artifacts` between jobs (current setup - it should be more reliable regarding deploy target, and won't share dev dependencies), a `before_script` with `npm install` is needed
* if `node_modules` is shared through `artifacts` (past setup, not recommended), `npm install` maybe part of the `build` job upon which other jobs depend.

## MIT License

Copyright (c) 2019 Guillaume Denis

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
